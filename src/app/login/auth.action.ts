

export class Login {
  static readonly type = '[Auth] Login';
  constructor(public user: string, public password: string ) {}
}

export class LoginSuccess {
  static readonly type = '[Auth] Login Success';
  constructor(public token: string) {}
}

export class Logout {
  static readonly type = '[Auth] Logout';
}


export class LoginFailed {
  static readonly type = '[Auth] Login Failed';
}
