import { State, Action, StateContext, Selector } from '@ngxs/store';

export enum MsgType {
  success = 1,
  warning = 2,
  error = 3
}
export class Msg {
  static readonly type = '[Msg] msg';
  constructor(public type: MsgType, public message: string, public path?: string) {}
}

export class MsgStateModel {
  type: MsgType;
  message: string;
  path?: string;
  msgDt: Date;
}

@State<MsgStateModel>({
  name: 'messages',
  defaults: {
    type: null,
    message: null,
    msgDt: null
  }
})

export class MsgState {

  @Selector() static selMsg( state: MsgStateModel) {
    return state;
  }
  @Action(Msg)
  msg( ctx: StateContext<MsgStateModel>, action: Msg ) {
    ctx.setState({
      type: action.type,
      message: action.message,
      path: action.path,
      msgDt: new Date()
    });
  }
}
