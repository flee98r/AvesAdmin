import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { TrackerService } from '../tracker.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  portfolioEditForm: FormGroup;

  constructor(private project: TrackerService) { }

  ngOnInit() {
    console.log(this.project);
    this.initForm();
  }

  initForm() {
    this.portfolioEditForm = new FormGroup({
      gold: new FormControl(this.project.gold, [
        Validators.required,
        Validators.minLength(4)
      ]),
      amp: new FormControl(this.project.amp, [
        Validators.required,
        Validators.minLength(5)
      ])
    });

    this.portfolioEditForm.valueChanges.pipe(debounceTime(500)).subscribe( ( values ) => {
        console.log( values );
        this.project.update( values );
        console.log(this.project);
    });


  }

}
