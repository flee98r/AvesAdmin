import { Audit } from '../../shared/shared.module';


export class Address extends Audit {

  public id: number;
  public name: string;
  public isDeleted = false;
  public addressLine1: string;
  public addressLine2: string;
  public zipcode: number;
  public city: string;
  public state: string;

  constructor( id: number, name: string, who: string) {
    super();
    this.id = id;
    this.name = name;
    this.createdBy = who;
    this.createdTs = new Date();
  }

}


