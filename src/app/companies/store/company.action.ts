import { Company } from './company.model';

export class AddCompany {
  static readonly type = '[Company] Add';
  constructor( public payload: Company ) {}
}

export class UpdateCompany {
  static readonly type = '[Company] Update';
  constructor( public payload: Company ) {}
}

export class LoadCompany {
  static readonly type = '[Company] Load';
  constructor( public payload: number ) {}
}

export class LoadAllCompany {
  static readonly type = '[Company] Load All';
}

export class DeleteCompany {
  static readonly type = '[Company] Delete';
  constructor( public id: number ) {}
}

export class LoadCompanySuccess {
  static readonly type = '[Company] Load Success';
  constructor( public payload: Company ) {}
}

export class ActionFailed {
  static readonly type = '[Company] Failure';
  constructor( public payload: Error ) {}
}
