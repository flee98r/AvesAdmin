import { State, Action, StateContext, Store } from '@ngxs/store';
import { Company } from './company.model';
import { AddCompany, LoadCompany, LoadAllCompany, DeleteCompany, LoadCompanySuccess, ActionFailed, UpdateCompany } from './company.action';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, filter } from 'rxjs/operators';
import { Msg, MsgType } from '../../header/msg.state';
import { environment } from '../../../environments/environment.prod';

​

@Injectable()

export class CompanyStateModel {
  loaded: boolean;
  companies: Company[];
}

@State<CompanyStateModel>({
  name: 'companies',
  defaults: {
    loaded: false,
    // companies: [new Company(1, 'abc', 'flee'), new Company(2, 'xyz', 'flee')]
    companies: []
  }
})

export class CompanyState {

  apiBaseUrl = environment.apiBaseUrl;

  constructor( private httpClient: HttpClient, private store: Store ) {}

  @Action(AddCompany)
  addCompany(ctx: StateContext<CompanyStateModel>, action: AddCompany ) {
    const state = ctx.getState();
    this.httpClient.post(this.apiBaseUrl + 'companies', action.payload, { responseType: 'text' } ).subscribe(
      (data: string) => {
        if ( data.indexOf( 'ok:') === 0 ) {
          const newId = +data.substring(3);
          this.store.dispatch( [new LoadCompany(newId), new Msg(MsgType.success, 'Company created!', '/companies')]);
        }
      }
    );


  }


  @Action(LoadCompany)
  loadCompany(ctx: StateContext<CompanyStateModel>, action: LoadCompany ) {
    const id: number = +action.payload;
    this.httpClient.get<Company>(this.apiBaseUrl + 'companies/' + id).subscribe(
      (company) => {

        const state = ctx.getState();
        // filter out existing.
        let idx = 0;
        const clone = state.companies.slice(0);

        const found = clone.some( (c) => {
          if (c.id === id) {
            clone[idx] = company;
            return true;
          }
          idx++;
        });
        if ( !found ) {
          clone.push(company);
        }

        ctx.patchState( {
          companies: clone
        });
        this.store.dispatch( [ new LoadCompanySuccess(company)]);

      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, error.message));
      }
    );
  }

  // @Action(LoadCompanySuccess)
  // loadCompanySuccess( ctx: StateContext<CompanyStateModel>, action: LoadCompanySuccess) {
  //   console.log("load Success");
  // }


  @Action(LoadAllCompany)
  loadAllCompany( ctx: StateContext<CompanyStateModel>) {

    // this.httpClient.get<Company[]>(this.apiBaseUrl + 'companies').subscribe(
    //   (companies) => {
    //     ctx.setState( {
    //       ...ctx.getState(),
    //       loaded: true,
    //       companies: companies
    //     });
    //   }
    // );

    this.httpClient.get<Company[]>(this.apiBaseUrl + 'companies').pipe(
      tap( (companies) => {
        ctx.setState( {
          ...ctx.getState(),
          loaded: true,
          companies: companies
        });
      }
    )).subscribe();

  }

  @Action(DeleteCompany)
  deleteCompany( ctx: StateContext<CompanyStateModel>, action: DeleteCompany) {

    this.httpClient.delete(this.apiBaseUrl + 'companies/' + action.id, { responseType: 'text'} ).subscribe(
      (result) => {
        ctx.setState( {
          ...ctx.getState(),
          companies: ctx.getState().companies.filter( (company) => company.id !== action.id )
        });
      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );

  }

  @Action(UpdateCompany)
  updateCompany( ctx: StateContext<CompanyStateModel>, action: UpdateCompany ) {

    const company = action.payload;
    this.httpClient.put(
      this.apiBaseUrl + 'companies/' +  company.id,
      company,
      { responseType: 'text' }
    ).subscribe(
      () => {
        // success... time to update my store.
        const state = ctx.getState();
        let idx = 0;
        const clone = state.companies.slice(0);
        clone.some( (c) => {
          if (c.id === +action.payload.id) {
            clone[idx] = action.payload;
            return true;
          }
          idx++;
        });
        ctx.setState( {
          ...ctx.getState(),
          companies: clone
        });
        this.store.dispatch(new Msg(MsgType.success, 'Company is updated.', '/companies'));

      },
      error => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );
  }



}
