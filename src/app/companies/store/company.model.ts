import { Audit } from '../../shared/shared.module';


export class Company extends Audit {

  public id: number;
  public name: string;
  public isDeleted = false;

  constructor( id: number, name: string, who: string) {
    super();
    this.id = id;
    this.name = name;
    this.createdBy = who;
    this.createdTs = new Date();
  }

}



