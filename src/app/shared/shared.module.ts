export abstract class Audit {
  public createdBy: string;
  public createdTs: Date;
}
