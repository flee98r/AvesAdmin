import { Component, OnInit } from '@angular/core';
import { InitColModel, ColumnModel } from './column.model';
import { FormGroup, FormArray, FormControl } from '@angular/forms';


@Component({
  selector: 'app-column-selection',
  templateUrl: './column-selection.component.html',
  styleUrls: ['./column-selection.component.css']
})
export class ColumnSelectionComponent implements OnInit {

  selColForm: FormGroup;
  Spreadsheet: string[] = [];
  GroupedColumn = {};
  col_def = [];
  constructor() {

    const columns: ColumnModel[] = new InitColModel().getColumns();
    // for ( let i = 0; i < 50; i++ ) {
    //   this.cols.push(i);
    // }

    for ( const col of columns ) {
      if ( !this.GroupedColumn[col.spreadsheet] ) {
        this.GroupedColumn[col.spreadsheet] = [];
        this.Spreadsheet.push(col.spreadsheet);
      }
      this.GroupedColumn[col.spreadsheet].push(col);
    }

    console.log( this.GroupedColumn );

    for ( const spd of Object.keys(this.GroupedColumn)) {
      this.col_def.push( { spd: spd, col_name: spd, type: 'p' });
      for ( const col of this.GroupedColumn[spd] ) {
        this.col_def.push(  { spd: col.spreadsheet, col_name: col.column_name, type: 'c' } );
      }
    }
    console.log( this.col_def );
  }

  ngOnInit() {

    const selArray = [];

    for ( const col of this.col_def ) {
      selArray.push( new FormControl(false) );
    }

    this.selColForm = new FormGroup( {
      'columns': new FormArray(selArray)
    });

    // this.selColForm.valueChanges.subscribe( (change) => {
    //   console.log(change);
    // });

  }

  onChange( idx ) {
    const ch_col = this.col_def[idx];
    const val =  this.selColForm.value.columns[idx];
    if ( this.col_def[idx].type === 'p' ) {
      for ( let i = 0; i < this.col_def.length; i++ ) {
        const col = this.col_def[i];
        if ( idx !== i && col.spd === ch_col.spd ) {
          (<FormArray>this.selColForm.get('columns')).controls[i].setValue(val);
        }
      }
    }
  }

}
