import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { AuthState } from './login/auth.state';



@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.store.selectSnapshot((state) => state.auth.token);
    console.log( 'Current Token: ' + token );
    if ( token ) {
      const copiedReq = req.clone({headers: req.headers.set('Authorization', token)});
      return next.handle(copiedReq);
    }
    return next.handle(req);

  }
}
