import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxsModule } from '@ngxs/store';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CompaniesComponent } from './companies/companies.component';
import { LocationsComponent } from './locations/locations.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';

import { CompanyState } from './companies/store/company.state';
import { AuthState } from './login/auth.state';
import { CompanyEditComponent } from './companies/company-edit/company-edit.component';
import { LocationEditComponent } from './locations/location-edit/location-edit.component';
import { MsgState } from './header/msg.state';
import { AuthInterceptor } from './auth.interceptor';
import { CanDeactivateGuard } from './can-deactivate-guard';

import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { LocationState } from './locations/store/location.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { AgGridModule } from 'ag-grid-angular';
import { TrackerComponent } from './tracker/tracker.component';
import { PortfolioComponent } from './tracker/portfolio/portfolio.component';
import { SprintComponent } from './tracker/sprint/sprint.component';
import { TrackerService } from './tracker/tracker.service';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { ColumnSelectionComponent } from './column-selection/column-selection.component';
​

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CompaniesComponent,
    LocationsComponent,
    LoginComponent,
    CompanyEditComponent,
    LocationEditComponent,
    TrackerComponent,
    PortfolioComponent,
    SprintComponent,
    ColumnSelectionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxsModule.forRoot([
      CompanyState,
      LocationState,
      AuthState,
      MsgState
    ]),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(
      {
        timeOut: 2000,
        positionClass: 'toast-bottom-right',
        preventDuplicates: true,
      }
    ), // ToastrModule added
    ModalModule.forRoot(),
    BootstrapModalModule,
    NgxsStoragePluginModule.forRoot({
      key: 'auth.token'
    }),

    NgxsReduxDevtoolsPluginModule.forRoot(),
    AgGridModule.withComponents([])
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    CanDeactivateGuard, TrackerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
